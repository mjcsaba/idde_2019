package edu.ubb.cs.idde.repository;

public abstract class DAOFactory {
    public static DAOFactory getInstance()
    {
        return new JPADAOFactory();
    }
    public abstract StudentDAO getStudentDAO();
}
