package edu.ubb.cs.idde.repository;

public class HibernateDAOFactory extends DAOFactory {

    public HibernateDAOFactory() {}

    @Override
    public StudentDAO getStudentDAO()
    {
        return new StudentDAOImpl();
    }
}
