package edu.ubb.cs.idde.model;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "students")
public class Student implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "student_name")
    private String name;
    @Column(name = "student_year")
    private Integer year;
    @Column(name = "nickname")
    private String nickname;

    public Student(){}

    public Student(Integer id, String name, Integer year, String nickname) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.nickname = nickname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
