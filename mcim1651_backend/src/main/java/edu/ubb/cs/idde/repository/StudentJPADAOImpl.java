package edu.ubb.cs.idde.repository;

import edu.ubb.cs.idde.model.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

public class StudentJPADAOImpl implements StudentDAO {

    private static EntityManagerFactory factory;
    private static final String PERSISTENCE_UNIT_NAME = "manager";
    private EntityManager entityManager;
    private static Logger logger = LoggerFactory.getLogger(StudentJPADAOImpl.class);

    public StudentJPADAOImpl() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        entityManager = factory.createEntityManager();
    }

    @Override
    public List<Student> listAllStudents() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Student> cq = cb.createQuery(Student.class);
        Root<Student> rootEntry = cq.from(Student.class);
        CriteriaQuery<Student> all = cq.select(rootEntry);
        TypedQuery<Student> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public boolean insert(Student student) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(student);
            entityManager.flush();
            System.out.println(">>>>>>>>>>>>>>>>" + entityManager.contains(student) + "");
            entityManager.getTransaction().commit();
        } catch (PersistenceException | IllegalArgumentException p) {
            entityManager.getTransaction().rollback();
            p.getStackTrace();
        }
        return true;
    }

    @Override
    public boolean update(Student student) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(student);
            entityManager.getTransaction().commit();
        } catch (PersistenceException | IllegalArgumentException p) {
            entityManager.getTransaction().rollback();
            p.getStackTrace();
        }
        return true;
    }

    @Override
    @Transactional
    public boolean delete(Student student) {
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(student);
            entityManager.getTransaction().commit();
        } catch (PersistenceException | IllegalArgumentException p) {
            entityManager.getTransaction().rollback();
            p.getStackTrace();
        }
        return true;
    }
}
