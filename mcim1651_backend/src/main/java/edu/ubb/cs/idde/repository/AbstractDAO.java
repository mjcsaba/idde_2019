package edu.ubb.cs.idde.repository;

import java.util.List;

public abstract class AbstractDAO<T> implements StudentDAO {

public abstract List<T> listAll();
public abstract boolean insert(T element);
public abstract boolean update(T newElement,T oldElement);
public abstract boolean delete(T toBeDeleted);
}
