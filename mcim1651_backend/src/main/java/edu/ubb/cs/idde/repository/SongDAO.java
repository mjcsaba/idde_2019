package edu.ubb.cs.idde.repository;

import edu.ubb.cs.idde.model.Song;

import java.util.List;

public interface SongDAO {
    List<Song> listAllSongs();

    //Crud
    boolean insert(Song student);
    boolean update(Song oldStudent);
    boolean delete(Song student);

}
