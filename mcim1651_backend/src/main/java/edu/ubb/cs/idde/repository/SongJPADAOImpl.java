package edu.ubb.cs.idde.repository;

import edu.ubb.cs.idde.model.Song;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

public class SongJPADAOImpl implements SongDAO {
    private static EntityManagerFactory factory;
    private static final String PERSISTENCE_UNIT_NAME = "manager";
    private EntityManager entityManager;

    public SongJPADAOImpl() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        entityManager = factory.createEntityManager();
    }

    @Override
    @Transactional
    public List<Song> listAllSongs() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Song> cq = cb.createQuery(Song.class);
        Root<Song> rootEntry = cq.from(Song.class);
        CriteriaQuery<Song> all = cq.select(rootEntry);
        TypedQuery<Song> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    @Transactional
    public boolean insert(Song Song) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(Song);
            entityManager.flush();
            System.out.println(">>>>>>>>>>>>>>>>" + entityManager.contains(Song) + "");
            entityManager.getTransaction().commit();
        } catch (PersistenceException | IllegalArgumentException p) {
            entityManager.getTransaction().rollback();
            p.getStackTrace();
        }
        return true;
    }

    @Override
    @Transactional
    public boolean update(Song Song) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(Song);
            entityManager.getTransaction().commit();
        } catch (PersistenceException | IllegalArgumentException p) {
            entityManager.getTransaction().rollback();
            p.getStackTrace();
        }
        return true;
    }

    @Override
    @Transactional
    public boolean delete(Song Song) {
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(Song);
            entityManager.getTransaction().commit();
        } catch (PersistenceException | IllegalArgumentException p) {
            entityManager.getTransaction().rollback();
            p.getStackTrace();
        }
        return true;
    }
}
