package edu.ubb.cs.idde.repository;

import edu.ubb.cs.idde.model.Person;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

public class PersonJPADAOImpl implements PersonDAO {
    private static final String PERSISTENCE_UNIT_NAME = "manager";
    private static EntityManagerFactory factory;
    private EntityManager entityManager;

    public PersonJPADAOImpl() {
        factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        entityManager = factory.createEntityManager();
    }

    @Override
    @Transactional
    public List<Person> listAllPersons() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Person> cq = cb.createQuery(Person.class);
        Root<Person> rootEntry = cq.from(Person.class);
        CriteriaQuery<Person> all = cq.select(rootEntry);
        TypedQuery<Person> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    @Transactional
    public boolean insert(Person Person) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(Person);
            entityManager.flush();
            System.out.println(">>>>>>>>>>>>>>>>" + entityManager.contains(Person) + "");
            entityManager.getTransaction().commit();
        } catch (PersistenceException | IllegalArgumentException p) {
            entityManager.getTransaction().rollback();
            p.getStackTrace();
        }
        return true;
    }

    @Override
    @Transactional
    public boolean update(Person Person) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(Person);
            entityManager.getTransaction().commit();
        } catch (PersistenceException | IllegalArgumentException p) {
            entityManager.getTransaction().rollback();
            p.getStackTrace();
        }
        return true;
    }

    @Override
    @Transactional
    public boolean delete(Person Person) {
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(Person);
            entityManager.getTransaction().commit();
        } catch (PersistenceException | IllegalArgumentException p) {
            entityManager.getTransaction().rollback();
            p.getStackTrace();
        }
        return true;
    }
}
