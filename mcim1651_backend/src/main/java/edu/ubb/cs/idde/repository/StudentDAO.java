package edu.ubb.cs.idde.repository;

import edu.ubb.cs.idde.model.Student;

import java.util.List;

public interface StudentDAO {
    List<Student> listAllStudents();

    //Crud
    boolean insert(Student student);
    boolean update(Student oldStudent);
    boolean delete(Student student);
}
