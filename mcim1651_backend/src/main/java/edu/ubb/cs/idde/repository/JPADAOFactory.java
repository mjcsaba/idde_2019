package edu.ubb.cs.idde.repository;

public class JPADAOFactory extends DAOFactory {
    public JPADAOFactory() {
    }

    @Override
    public StudentDAO getStudentDAO() {
        return new StudentJPADAOImpl();
    }
}
