package edu.ubb.cs.idde.repository;


import edu.ubb.cs.idde.model.Person;

import java.util.List;

public interface PersonDAO {
    List<Person> listAllPersons();

    //Crud
    boolean insert(Person student);
    boolean update(Person oldStudent);
    boolean delete(Person student);

}
