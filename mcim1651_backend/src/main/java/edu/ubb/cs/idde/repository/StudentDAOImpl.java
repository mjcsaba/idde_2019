package edu.ubb.cs.idde.repository;

import com.sun.jmx.mbeanserver.Repository;
import edu.ubb.cs.idde.model.Student;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class StudentDAOImpl implements StudentDAO {
    private static SessionFactory sessionFactory;

    public StudentDAOImpl() {
        sessionFactory= new Configuration().configure().buildSessionFactory();
    }

    @Override
    public List<Student> listAllStudents() {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        List<Student> allStudents = new LinkedList<>();
        try {
            transaction = session.beginTransaction();
            List students = session.createQuery(new String("FROM Student")).list();
            System.out.println(students + "");
            for (Iterator iterator = students.iterator(); iterator.hasNext(); ) {
                Student student = (Student) iterator.next();
                allStudents.add(student);
            }
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
            throw new RuntimeException();
        } finally {
            session.close();
        }
        return allStudents;
    }

    // Hibarnate implementation not needed CRUD methods
    @Override
    public boolean insert(Student student) {
        System.out.println("NEM EZ KELL");
        return true;
    }

    @Override
    public boolean update(Student oldStudent) {
        return false;
    }

    @Override
    public boolean delete(Student student) {
        return false;
    }

  }
