package edu.ubb.cs.idde;

import edu.ubb.cs.idde.model.Student;
import edu.ubb.cs.idde.repository.DAOFactory;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.*;
import java.util.List;

public class Window extends JFrame {
    private JButton listAll;
    private JPanel contentPanel;
    private JTable table;
    private JButton hu;
    private JButton en;
    private List<Student> students; 
    public Window(){

        //setting up the languages
        ResourceBundle resourceBundle= null;
        try {
            Locale locale = new Locale("en");
            resourceBundle = ResourceBundle.getBundle("Label",locale);
        }
        catch (MissingResourceException ex)
        {
            ex.printStackTrace();
            System.out.println("Labels not found");
        }
        String showStudentButtonText = resourceBundle.getString("showData");

        //the Window contents
        listAll = new JButton(showStudentButtonText);
        hu = new JButton("Hun");
        en = new JButton("En");
        contentPanel = new JPanel();

        // Reflection for table header
        List<String> InitialColumns = new ArrayList<>();
        try{
            Class clazz = Class.forName(Student.class.getName());
            Field[] fields = clazz.getDeclaredFields();
            int i=0;
            for(Field field : fields)
            {
                InitialColumns.add(field.getName());
            }
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        String columns[]=new String[InitialColumns.size()];
        for(int i=0;i<InitialColumns.size();i++){
            columns[i] = resourceBundle.getString(InitialColumns.get(i));
        }

        students = DAOFactory.getInstance().getStudentDAO().listAllStudents();
        DefaultTableModel defaultTableModel = new DefaultTableModel();
        defaultTableModel.setColumnIdentifiers(columns);
        table = new JTable();
        table.setModel(defaultTableModel);
        for (int i = 0; i < students.size(); i++) {
            defaultTableModel.addRow(new Object[]{students.get(i).getName(),students.get(i).getId(),students.get(i).getNickname(),students.get(i).getYear()});
        }
        contentPanel.add(new JScrollPane(table), BorderLayout.CENTER);
        table.setVisible(false);
        contentPanel.add(listAll, BorderLayout.NORTH);
        contentPanel.add(hu,BorderLayout.NORTH);
        contentPanel.add(en,BorderLayout.NORTH);
        listAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.setVisible(true);

            }
        });

        // change to hungarian language
        hu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Locale locale = new Locale("hu");
                ResourceBundle resourceBundle = ResourceBundle.getBundle("Label",locale);
                listAll.setText(resourceBundle.getString("showData"));

                String [] columns = {resourceBundle.getString("name"),resourceBundle.getString("id"),resourceBundle.getString("year"),resourceBundle.getString("nickname")};
                DefaultTableModel defaultTableModel = new DefaultTableModel();
                defaultTableModel.setColumnIdentifiers(columns);
                for (int i = 0; i < students.size(); i++) {
                    defaultTableModel.addRow(new Object[]{students.get(i).getName(),students.get(i).getId(),students.get(i).getNickname(),students.get(i).getYear()});
                }
                table.setModel(defaultTableModel);
            }
        });

        // change to english language
        en.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Locale locale = new Locale("en");
                ResourceBundle resourceBundle = ResourceBundle.getBundle("Label",locale);
                listAll.setText(resourceBundle.getString("showData"));

                String [] columns = {resourceBundle.getString("name"),resourceBundle.getString("id"),resourceBundle.getString("year"),resourceBundle.getString("nickname")};
                DefaultTableModel defaultTableModel = new DefaultTableModel();
                defaultTableModel.setColumnIdentifiers(columns);
                for (int i = 0; i < students.size(); i++) {
                    defaultTableModel.addRow(new Object[]{students.get(i).getName(),students.get(i).getId(),students.get(i).getNickname(),students.get(i).getYear()});
                }
                table.setModel(defaultTableModel);
            }
        });
        this.setContentPane(contentPanel);
        Student student=new Student();
        student.setName("TEST");
        student.setNickname("TEST");
        student.setYear(1996);
        DAOFactory.getInstance().getStudentDAO().insert(student);
    }

}
