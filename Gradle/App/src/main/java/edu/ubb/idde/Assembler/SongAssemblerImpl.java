package edu.ubb.idde.Assembler;

import DTO.SongDTO;
import edu.ubb.idde.model.Song;

public class SongAssemblerImpl implements SongAssembler {
    @Override
    public SongDTO modelToDto(Song student) {
        SongDTO songDTO = new SongDTO(student.getId(),student.getName(),student.getArtist());
    return songDTO;
    }

    @Override
    public Song DtoToModel(SongDTO studentDTO) {
        Song song = new Song(studentDTO.getName(),studentDTO.getArtist());
        song.setId(studentDTO.getId());
        return song;
    }
}
