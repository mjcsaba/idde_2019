package edu.ubb.idde.Controllers;

import edu.ubb.idde.model.Person;
import edu.ubb.idde.services.IPersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/persons")
public class PersonController {
    private static Logger logger= LoggerFactory.getLogger(PersonController.class);
    @Autowired
    IPersonService IPersonService;

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Person getPerson(@PathVariable Long id){
        Person person = new Person();
        try{
            person = IPersonService.findById(id);
        }
        catch (DataAccessException ex){
            logger.error(ex.toString());
        }
        return person;
    }

    @PostMapping
    @ResponseBody
    public Boolean addPerson(){
        Person person = new Person();
        person.setName("Leonardo");
        person.setAge(112);
        IPersonService.insert(person);
        return true;
    }

    @GetMapping
    @ResponseBody
    public List<Person> getPersons(){
        List<Person> persons = new LinkedList<>();
        try{
            persons = IPersonService.findAll();
        }
        catch (DataAccessException e){
            logger.error(e.toString());
        }
        return persons;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deletePerson(@PathVariable Long id){
        Person person = getPerson(id);
        try {
            IPersonService.deletePerson(person);
        }
        catch (DataAccessException e){
            logger.error(e.toString());
        }
        return true;
    }
}
