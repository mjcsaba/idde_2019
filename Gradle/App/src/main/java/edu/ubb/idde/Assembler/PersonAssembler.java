package edu.ubb.idde.Assembler;

import DTO.PersonDTO;
import edu.ubb.idde.model.Person;

public interface PersonAssembler {
    PersonDTO modelToDto(Person person);
    Person DtoToModel(PersonDTO studentDTO);
}
