package edu.ubb.idde.Assembler;

import DTO.StudentDTO;
import edu.ubb.idde.model.Student;

public class StudentAssemblerImpl implements StudentAssembler {
    @Override
    public StudentDTO modelToDto(Student student) {
        StudentDTO studentDTO = new StudentDTO();
        studentDTO.setId(student.getId());
        studentDTO.setName(student.getName());
        studentDTO.setNickname(student.getNickname());
        studentDTO.setYear(student.getYear());
        return studentDTO;
    }

    @Override
    public Student DtoToModel(StudentDTO studentDTO) {
        Student student = new Student(studentDTO.getId(),studentDTO.getYear(),studentDTO.getName(),studentDTO.getNickname());
        return student;
    }
}
