package edu.ubb.idde.Assembler;

import DTO.StudentDTO;
import edu.ubb.idde.model.Student;

public interface StudentAssembler {
    StudentDTO modelToDto(Student student);
    Student DtoToModel(StudentDTO studentDTO);
}
