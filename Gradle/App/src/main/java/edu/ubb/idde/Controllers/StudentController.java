package edu.ubb.idde.Controllers;

import edu.ubb.idde.model.Student;
import edu.ubb.idde.services.IStudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "/students")
public class StudentController {
    private static Logger logger= LoggerFactory.getLogger(PersonController.class);
    @Autowired
    IStudentService IStudentService;

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Student getStudent(@PathVariable Long id){
        Student student = new Student();
        try{
            student = IStudentService.findById(id);
        }
        catch (DataAccessException ex){
            logger.error(ex.toString());
        }
        return student;
    }

    @PostMapping
    @ResponseBody
    public Boolean addStudent(){
        Student student = new Student();
        student.setName("SpringBoot");
        student.setNickname("Booot");
        student.setYear(12345);
        IStudentService.insert(student);
        return true;
    }

    @GetMapping
    @ResponseBody
    public List<Student> getStudents(){
        List<Student> students = new LinkedList<>();
        try{
            students = IStudentService.findAll();
        }
        catch (DataAccessException e){
            logger.error(e.toString());
        }
        return students;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deleteStudent(@PathVariable Long id){
        Student student = getStudent(id);
        try {
            IStudentService.deleteStudent(student);
        }
        catch (DataAccessException e){
            logger.error(e.toString());
        }
        return true;
    }
}
