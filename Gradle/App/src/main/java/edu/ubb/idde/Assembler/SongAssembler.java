package edu.ubb.idde.Assembler;

import DTO.SongDTO;
import edu.ubb.idde.model.Song;

public interface SongAssembler {
    SongDTO modelToDto(Song student);
    Song DtoToModel(SongDTO studentDTO);

}
