package edu.ubb.idde.Controllers;


import edu.ubb.idde.model.Song;
import edu.ubb.idde.services.ISongService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "/songs")
public class SongController {
    private static Logger logger= LoggerFactory.getLogger(PersonController.class);
    @Autowired
    ISongService ISongService;

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    @ResponseBody
    public Song getSong(@PathVariable Long id){
        Song song = new Song();
        try{
            song = ISongService.findById(id);
        }
        catch (DataAccessException ex){
            logger.error(ex.toString());
        }
        return song;
    }

    @PostMapping
    @ResponseBody
    public Boolean addSong(){
        Song Song = new Song();
        Song.setName("Leonardo");
        Song.setArtist("DaVinci");
        ISongService.insert(Song);
        return true;
    }

    @GetMapping
    @ResponseBody
    public List<Song> getSongs(){
        List<Song> songs = new LinkedList<>();
        try{
            songs = ISongService.findAll();
        }
        catch (DataAccessException e){
            logger.error(e.toString());
        }
        return songs;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean deleteSong(@PathVariable Long id){
        Song song = getSong(id);
        try {
            ISongService.deleteSong(song);
        }
        catch (DataAccessException e){
            logger.error(e.toString());
        }
        return true;
    }


}
