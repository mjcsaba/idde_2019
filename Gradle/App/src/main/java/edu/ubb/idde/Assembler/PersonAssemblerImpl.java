package edu.ubb.idde.Assembler;

import DTO.PersonDTO;
import edu.ubb.idde.model.Person;
import org.springframework.stereotype.Service;

@Service
public class PersonAssemblerImpl implements  PersonAssembler{
    @Override
    public PersonDTO modelToDto(Person person) {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setAge(person.getAge());
        personDTO.setId(person.getId());
        personDTO.setName(person.getName());

        return personDTO;
    }

    @Override
    public Person DtoToModel(PersonDTO personDTO) {
        Person person = new Person();
        person.setAge(personDTO.getAge());
        person.setName(personDTO.getName());
        person.setId(personDTO.getId());

        return person;
    }
}
