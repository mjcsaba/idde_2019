package edu.ubb.idde.model;


import javax.persistence.*;

@Entity(name = "Students")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "student_year")
    private int year;

    @Column(name = "student_name")
    private String name;

    private String nickname;

    public Student(Long id, int year, String name, String nickname) {
        this.id = id;
        this.year = year;
        this.name = name;
        this.nickname = nickname;
    }

    public Student() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
