package edu.ubb.idde.services;

import edu.ubb.idde.model.Person;

import java.util.List;

public interface PersonService extends EntityService<Person>{
}
