package edu.ubb.idde.services;

import edu.ubb.idde.model.Song;

import java.util.List;

public interface SongService extends EntityService<Song>{

}
