package edu.ubb.idde.services;

import java.util.List;

public interface EntityService<T> {
     T findById(Long id);
     void insert(T person);
     void update(T person);
     List<T> findAll();
     void deletePerson(T person);
}
