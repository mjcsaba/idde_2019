package edu.ubb.idde.services;

import edu.ubb.idde.model.Student;
import edu.ubb.idde.repositories.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class IStudentService implements StudentService{
    Logger logger = LoggerFactory.getLogger(ISongService.class);

    @Autowired
    private StudentRepository studentRepository;

    public Student findById(Long id){
        Student student = new Student();
        try{
            if(studentRepository.findById(id).isPresent()){
                student=studentRepository.findById(id).get();
            }
        }
        catch (DataAccessException ex){
            logger.error(ex.toString());
        }
        return student;
    }

    public void insert(Student student){
        studentRepository.save(student);
    }

    public void update(Student student){
        studentRepository.save(student);
    }

    public List<Student> findAll(){
        List<Student> students = new LinkedList<>();
        studentRepository.findAll().forEach(students::add);
        return  students;
    }

    @Override
    public void deletePerson(Student person) {

    }

    public void deleteStudent(Student student) { studentRepository.delete(student); }

}
