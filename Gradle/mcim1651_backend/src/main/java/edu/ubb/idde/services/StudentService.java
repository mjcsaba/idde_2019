package edu.ubb.idde.services;

import edu.ubb.idde.model.Student;

import java.util.List;

public interface StudentService extends EntityService<Student>{
   }
