package edu.ubb.idde.services;

import edu.ubb.idde.model.Person;
import edu.ubb.idde.repositories.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class IPersonService implements PersonService{
    Logger logger = LoggerFactory.getLogger(ISongService.class);
    @Autowired
    private PersonRepository personRepository;

    public Person findById(Long id){
        Person person = new Person();
        try {
            if (personRepository.findById(id).isPresent()){
                person = personRepository.findById(id).get();
            }
        }
        catch (DataAccessException ex){
            logger.error(ex.toString());
        }
        return person;
    }

    public void insert(Person person){
        personRepository.save(person);
    }

    public void update(Person person){personRepository.save(person);}

    public List<Person> findAll(){
        List<Person> persons = new LinkedList<>();
        personRepository.findAll().forEach(persons::add);
        return  persons;
    }

    public void deletePerson(Person person) {personRepository.delete(person);}

}
