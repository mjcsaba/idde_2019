package edu.ubb.idde.services;

import edu.ubb.idde.model.Song;
import edu.ubb.idde.repositories.SongRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class ISongService implements SongService{
    Logger logger = LoggerFactory.getLogger(ISongService.class);
    @Autowired
    private SongRepository songRepository;

    public Song findById(Long id){
        Song song = new Song();
        try{
            if(songRepository.findById(id).isPresent()){
                song=songRepository.findById(id).get();
            }

        }
        catch (DataAccessException ex){
           logger.error(ex.toString());
        }
        return song;
    }

    public void insert(Song song){
        songRepository.save(song);
    }

    public void update(Song song){songRepository.save(song);}

    public List<Song> findAll(){
        List<Song> songs = new LinkedList<>();
        songRepository.findAll().forEach(songs::add);
        return  songs;
    }

    @Override
    public void deletePerson(Song person) {

    }

    public void deleteSong(Song song) {songRepository.delete(song);}

}
