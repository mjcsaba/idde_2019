import DTO.PersonDTO;
import DTO.SongDTO;
import DTO.StudentDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.*;
import java.util.List;

public class Window extends JFrame {
    private JButton listAll;
    private JPanel contentPanel;
    private JTable table;
    private StudentDTO[] studentDTOS;
    private PersonDTO[] personDTOS;
    private SongDTO[] songDTOS;
    private JButton students;
    private JButton songs;
    private JButton persons;
    private DefaultTableModel defaultTableModel;
    private ResourceBundle resourceBundle;
    private static Logger logger= LoggerFactory.getLogger(Window.class);

    public Window(){

        try {
            Locale locale = new Locale("en");
            resourceBundle = ResourceBundle.getBundle("Label",locale);
        }
        catch (MissingResourceException ex)
        {
            ex.printStackTrace();
            System.out.println("Labels not found");
        }


        students = new JButton(resourceBundle.getString("showStud"));
        songs = new JButton("List Songs");
        persons = new JButton("List People");
        contentPanel = new JPanel();

        //Sending the request to get all data

        defaultTableModel = new DefaultTableModel();
        table = new JTable();


        contentPanel.add(table, BorderLayout.CENTER);
        table.setVisible(false);
        contentPanel.add(students, BorderLayout.NORTH);
        contentPanel.add(songs, BorderLayout.EAST);
        contentPanel.add(persons, BorderLayout.WEST);
        students.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.repaint();
                table.setVisible(true);
                showEntity("student");
            }
        });



        songs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.repaint();
                table.setVisible(true);
                showEntity("song");

            }
        });

        persons.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.repaint();
                table.setVisible(true);
                    showEntity("person");
            }
        });
        this.setContentPane(contentPanel);
    }

    public void showEntity(String entity){
          if(entity.compareTo("student") == 0)
     {
         RestTemplate restTemplate = new RestTemplate();
         studentDTOS = restTemplate.getForObject("http://localhost:8080/students",StudentDTO[].class);
         String [] columns = {"Name","Id","Year","Nickname"};
         defaultTableModel.setColumnIdentifiers(columns);
         table.setModel(defaultTableModel);

         for(StudentDTO s: studentDTOS){
             defaultTableModel.addRow(new Object[]{s.getName(),s.getId(),s.getYear(),s.getNickname()});
         }
         defaultTableModel.fireTableDataChanged();
     }

     if(entity.compareTo("song") == 0)
     {
         RestTemplate restTemplate = new RestTemplate();
         defaultTableModel = new DefaultTableModel();
         songDTOS = restTemplate.getForObject("http://localhost:8080/songs",SongDTO[].class);
         String [] columns = {"Name","Id","Artist"};
         defaultTableModel.setColumnIdentifiers(columns);
         table.setModel(defaultTableModel);

         for(SongDTO s: songDTOS){
             defaultTableModel.addRow(new Object[]{s.getName(),s.getId(),s.getArtist()});
         }
         defaultTableModel.fireTableDataChanged();
     }

     if(entity.compareTo("person") == 0)
     {
         RestTemplate restTemplate = new RestTemplate();
         defaultTableModel = new DefaultTableModel();
         personDTOS = restTemplate.getForObject("http://localhost:8080/persons",PersonDTO[].class);

         //Reflection
         List<String> InitialColumns = new ArrayList<>();
         try{
             Class clazz = Class.forName(StudentDTO.class.getName());
             Field[] fields = clazz.getDeclaredFields();
             int i=0;
             for(Field field : fields)
             {
                 InitialColumns.add(field.getName());
             }
         }
         catch (ClassNotFoundException e)
         {
             logger.error(e.toString());
         }
         String columns[]=new String[InitialColumns.size()];
         for(int i=0;i<InitialColumns.size();i++){
             columns[i] = resourceBundle.getString(InitialColumns.get(i));
         }

         defaultTableModel.setColumnIdentifiers(columns);
         table.setModel(defaultTableModel);

         for(PersonDTO s: personDTOS){
             defaultTableModel.addRow(new Object[]{s.getName(),s.getId(),s.getAge()});
         }
         defaultTableModel.fireTableDataChanged();
     }
    }

}
